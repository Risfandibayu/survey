<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Komplain | <?php echo e(Utility::getsettings('app_name')); ?></title>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <link rel="icon"
        href="<?php echo e(Utility::getsettings('favicon_logo') ? Storage::url('app-logo/app-favicon-logo.png') : ''); ?>"
        type="image/png">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .mb-oo{
                margin-bottom: 300px
            }
        </style>
    </head>

    <body>
        <div class="mt-5 mb-oo d-flex justify-content-center align-items-center">
            
            <div class="card col-11 col-md-6">
                <div class="card-header px-5 py-4">
                    <h5> <?php echo e($formValue->Form->title); ?>


                    </h5>
                </div>
                
                <div class="card-body px-5 py-4">
                    <div class="view-form-data">
                        <div class="row">
                            <?php $__currentLoopData = $array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keys => $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = $rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_key => $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        // $fieldHidden = false;
                                        // if (in_array($row->name, $hideFields)) {
                                        //     $fieldHidden = true;
                                        // }
                                    ?>
                                    <?php if($row->type == 'checkbox-group'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <p>
                                                    <ul>
                                                        <?php $__currentLoopData = $row->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $options): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if(isset($options->selected)): ?>
                                                                <li>
                                                                    <label><?php echo e($options->label); ?></label>
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'file'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <p>
                                                        <?php if(property_exists($row, 'value')): ?>
                                                            <?php if($row->value): ?>
                                                                <?php
                                                                    $allowed_extensions = ['pdf', 'pdfa', 'fdf', 'xdp', 'xfa', 'pdx', 'pdp', 'pdfxml', 'pdxox', 'xlsx', 'csv', 'xlsm', 'xltx', 'xlsb', 'xltm', 'xlw'];
                                                                ?>
                                                                <?php if($row->multiple): ?>
                                                                    <div class="row">
                                                                        <?php if(App\Facades\UtilityFacades::getsettings('storage_type') == 'local'): ?>
                                                                            <?php $__currentLoopData = $row->value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <div class="col-6">
                                                                                    <?php
                                                                                        $fileName = explode('/', $img);
                                                                                        $fileName = end($fileName);
                                                                                    ?>
                                                                                    <?php if(in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions)): ?>
                                                                                        <?php
                                                                                            $fileName = explode('/', $img);
                                                                                            $fileName = end($fileName);
                                                                                        ?>
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="<?php echo e(asset('storage/app/' . $img)); ?>"
                                                                                            type="image"
                                                                                            download=""><?php echo substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : ''); ?></a>
                                                                                    <?php else: ?>
                                                                                        <img src="<?php echo e(Storage::exists($img) ? asset('storage/app/' . $img) : Storage::url('not-exists-data-images/78x78.png')); ?>"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnail">
                                                                                            <a href="<?php echo e(Storage::exists($img)); ?>" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php else: ?>
                                                                            <?php $__currentLoopData = $row->value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <div class="col-6">
                                                                                    <?php
                                                                                        $fileName = explode('/', $img);
                                                                                        $fileName = end($fileName);
                                                                                    ?>
                                                                                    <?php if(in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions)): ?>
                                                                                        <?php
                                                                                            $fileName = explode('/', $img);
                                                                                            $fileName = end($fileName);
                                                                                        ?>
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="<?php echo e(Storage::url($img)); ?>"
                                                                                            type="image"
                                                                                            download=""><?php echo substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : ''); ?></a>
                                                                                    <?php else: ?>
                                                                                        <img src="<?php echo e(Storage::url($img)); ?>"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnail">
                                                                                            <a href="<?php echo e(Storage::exists($img)); ?>" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                <?php else: ?>
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <?php if($row->subtype == 'fineuploader'): ?>
                                                                                <?php if(App\Facades\UtilityFacades::getsettings('storage_type') == 'local'): ?>
                                                                                    <?php if($row->value[0]): ?>
                                                                                        <?php $__currentLoopData = $row->value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                            <?php
                                                                                                $fileName = explode('/', $img);
                                                                                                $fileName = end($fileName);
                                                                                            ?>
                                                                                            <?php if(in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions)): ?>
                                                                                                <a class="my-2 btn btn-info"
                                                                                                    href="<?php echo e(asset('storage/app/' . $img)); ?>"
                                                                                                    type="image"
                                                                                                    download=""><?php echo substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : ''); ?></a>
                                                                                            <?php else: ?>
                                                                                                <img src="<?php echo e(Storage::exists($img) ? asset('storage/app/' . $img) : Storage::url('not-exists-data-images/78x78.png')); ?>"
                                                                                                    class="mb-2 img-responsive img-thumbnail">
                                                                                                    <a href="<?php echo e(asset('storage/app/' . $img)); ?>" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                            <?php endif; ?>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                    <?php endif; ?>
                                                                                <?php else: ?>
                                                                                    <?php if($row->value[0]): ?>
                                                                                        <?php $__currentLoopData = $row->value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                            <?php
                                                                                                $fileName = explode('/', $img);
                                                                                                $fileName = end($fileName);
                                                                                            ?>
                                                                                            <?php if(in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions)): ?>
                                                                                                <a class="my-2 btn btn-info"
                                                                                                    href="<?php echo e(Storage::url($img)); ?>"
                                                                                                    type="image"
                                                                                                    download=""><?php echo substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : ''); ?></a>
                                                                                            <?php else: ?>
                                                                                                <img src="<?php echo e(Storage::url($img)); ?>"
                                                                                                    class="mb-2 img-responsive img-fluid img-thumbnail">
                                                                                                    <a href="<?php echo e(Storage::exists($img)); ?>" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                            <?php endif; ?>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php else: ?>
                                                                                <?php if(App\Facades\UtilityFacades::getsettings('storage_type') == 'local'): ?>
                                                                                    <?php if(in_array(pathinfo($row->value, PATHINFO_EXTENSION), $allowed_extensions)): ?>
                                                                                        <?php
                                                                                            $fileName = explode('/', $row->value);
                                                                                            $fileName = end($fileName);
                                                                                        ?>
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="<?php echo e(asset('storage/app/' . $row->value)); ?>"
                                                                                            type="image"
                                                                                            download=""><?php echo substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : ''); ?></a>
                                                                                    <?php else: ?>
                                                                                        <img src="<?php echo e(Storage::exists($row->value) ? asset('storage/app/' . $row->value) : Storage::url('not-exists-data-images/78x78.png')); ?>"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnailss">
                                                                                            <a href="<?php echo e(asset('storage/app/' . $row->value)); ?>" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    <?php endif; ?>
                                                                                <?php else: ?>
                                                                                    <?php if(in_array(pathinfo($row->value, PATHINFO_EXTENSION), $allowed_extensions)): ?>
                                                                                        <?php
                                                                                            $fileName = explode('/', $row->value);
                                                                                            $fileName = end($fileName);
                                                                                        ?>
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="<?php echo e(Storage::url($row->value)); ?>"
                                                                                            type="image"
                                                                                            download=""><?php echo substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : ''); ?></a>
                                                                                    <?php else: ?>
                                                                                        <img src="<?php echo e(Storage::url($row->value)); ?>"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnailss">
                                                                                            <a href="<?php echo e(Storage::exists($row->value)); ?>" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'header'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <<?php echo e($row->subtype); ?>>
                                                        <?php echo e(html_entity_decode($row->label)); ?>

                                                        </<?php echo e($row->subtype); ?>>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'paragraph'): ?>
                                        
                                    <?php elseif($row->type == 'radio-group'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <p>
                                                        <?php $__currentLoopData = $row->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $options): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if(isset($options->selected)): ?>
                                                                <?php echo e($options->label); ?>

                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'select'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>

                                                    <p>
                                                        <?php $__currentLoopData = $row->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $options): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if(isset($options->selected)): ?>
                                                                <?php echo e($options->label); ?>

                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'autocomplete'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <p>
                                                        <?php echo e($row->value); ?>

                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'number'): ?>
                                        
                                            <div class="col-12">
                                                <b><?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                </b>
                                                <p>
                                                    <?php echo e(isset($row->value) ? $row->value : ''); ?>

                                                </p>
                                            </div>
                                        
                                    <?php elseif($row->type == 'text'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    
                                                    <?php if($row->subtype == 'color'): ?>
                                                        <div class="p-2"
                                                            style="background-color: <?php echo e($row->value); ?>;">
                                                        </div>
                                                    <?php else: ?>
                                                        <p style="font-weight: 700">
                                                            <?php echo e(isset($row->value) ? $row->value : ''); ?>

                                                        </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'date'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <p style="font-weight: 700">
                                                        <?php echo e(isset($row->value) ? date('d-m-Y', strtotime($row->value)) : ''); ?>

                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'textarea'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <?php if($row->subtype == 'ckeditor'): ?>
                                                        <?php echo isset($row->value) ? $row->value : ''; ?>

                                                    <?php else: ?>
                                                        <p style="font-weight: 700">
                                                            <?php echo e(isset($row->value) ? $row->value : ''); ?>

                                                        </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'starRating'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php
                                                        $attr = ['class' => 'form-control'];
                                                        if ($row->required) {
                                                            $attr['required'] = 'required';
                                                        }
                                                        $value = isset($row->value) ? $row->value : 0;
                                                        $noOfStar = isset($row->number_of_star) ? $row->number_of_star : 5;
                                                    ?>
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <p>
                                                    <div id="<?php echo e($row->name); ?>" class="starRating"
                                                        data-value="<?php echo e($value); ?>"
                                                        data-no_of_star="<?php echo e($noOfStar); ?>">
                                                    </div>
                                                    <input type="hidden" name="<?php echo e($row->name); ?>"
                                                        value="<?php echo e($value); ?>">
                                                    </p>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'SignaturePad'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php
                                                        $attr = ['class' => 'form-control'];
                                                        if ($row->required) {
                                                            $attr['required'] = 'required';
                                                        }
                                                        $value = isset($row->value) ? $row->value : 0;
                                                        $noOfStar = isset($row->number_of_star) ? $row->number_of_star : 5;
                                                    ?>
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?>

                                                    <?php if($row->required): ?>
                                                        <span class="text-danger align-items-center">*</span>
                                                    <?php endif; ?>
                                                    <?php if(property_exists($row, 'value')): ?>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <img src="<?php echo e(asset(Storage::url($row->value))); ?>">
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'break'): ?>
                                        
                                    <?php elseif($row->type == 'location'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo Form::label('location_id', 'Location:'); ?>

                                                    <iframe width="100%" height="260" frameborder="0"
                                                        scrolling="no" marginheight="0" marginwidth="0"
                                                        src="https://maps.google.com/maps?q=<?php echo e($row->value); ?>&hl=en&z=14&amp;output=embed">
                                                    </iframe>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'video'): ?>
                                        
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?><br>
                                                    <a href="<?php echo e(route('selfie.image.download', $formValue->id)); ?>">
                                                        <button class="btn btn-primary p-2"
                                                            id="downloadButton"><?php echo e(__('Download Video')); ?></button></a>
                                                </div>
                                            </div>
                                        
                                    <?php elseif($row->type == 'selfie'): ?>
                                        
                                            <div class="row">
                                                <div class="col-12">
                                                    <?php echo e(Form::label($row->name, $row->label, ['class' => 'form-label'])); ?><br>
                                                    <img
                                                        src=" <?php echo e(Illuminate\Support\Facades\File::exists(Storage::path($row->value)) ? Storage::url($row->value) : Storage::url('app-logo/78x78.png')); ?>"class="img-responsive img-thumbnailss mb-2">
                                                    <br>
                                                    <a href="<?php echo e(route('selfie.image.download', $formValue->id)); ?>">
                                                        <button class="btn btn-primary p-2"
                                                            id="downloadButton"><?php echo e(__('Download Image')); ?></button></a>
                                                </div>
                                            </div>
                                        
                                    <?php else: ?>
                                        
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
                <div class="card-footer px-5 py-4">
                    <form class="text-center" action="<?php echo e(route('forms.updatestatus.petugas.post',[$sid])); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <div class="form-group">
                          <label for="petugas" class="text-left">Petugas</label>
                          <input type="text" name="petugas" class="form-control" id="petugas" placeholder="Masukan Petugas">
                        </div>
                        <button type="submit" class="mt-2 btn btn-block btn-success">Simpan & Kerjakan</button>
                      </form>
                </div>
            </div>
            <script>
                $("button").click(function () {
  $(".check-icon").hide();
  setTimeout(function () {
    $(".check-icon").show();
  }, 10);
});
            </script>
    </body>

</html> <?php /**PATH C:\xampp\htdocs\prime\resources\views/form-value/petugas.blade.php ENDPATH**/ ?>