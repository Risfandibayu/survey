{{--
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Count Up Timer</title>
    <style>
        #timer {
            font-size: 24px;
            font-weight: bold;
        }
    </style>
</head>

<body>

    <div id="timer">{{$timer}}</div>

    <script>
        // Mendapatkan waktu awal dari div timer
  let timerDiv = document.getElementById("timer");
  let startTime = timerDiv.textContent.split(":").map(Number);
  let hours = startTime[0];
  let minutes = startTime[1];
  let seconds = startTime[2];

  // Fungsi untuk menambahkan nol di depan angka jika kurang dari 10
  function pad(num) {
    return (num < 10 ? "0" : "") + num;
  }

  // Fungsi untuk mengupdate timer setiap detik
  function updateTimer() {
    seconds++;
    if (seconds >= 60) {
      seconds = 0;
      minutes++;
      if (minutes >= 60) {
        minutes = 0;
        hours++;
      }
    }
    // Menampilkan timer dalam format HH:MM:SS
    timerDiv.textContent = pad(hours) + ":" + pad(minutes) + ":" + pad(seconds);
  }

  // Memulai timer
  setInterval(updateTimer, 1000);
    </script>

</body>

</html> --}}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Status Berhasil | {{ Utility::getsettings('app_name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
        href="{{ Utility::getsettings('favicon_logo') ? Storage::url('app-logo/app-favicon-logo.png') : '' }}"
        type="image/png">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        /**
 * Extracted from: SweetAlert
 * Modified by: Istiak Tridip
 */
        .success-checkmark {
            width: 80px;
            height: 115px;
            margin: 0 auto;

            .check-icon {
                width: 80px;
                height: 80px;
                position: relative;
                border-radius: 50%;
                box-sizing: content-box;
                border: 4px solid #4CAF50;

                &::before {
                    top: 3px;
                    left: -2px;
                    width: 30px;
                    transform-origin: 100% 50%;
                    border-radius: 100px 0 0 100px;
                }

                &::after {
                    top: 0;
                    left: 30px;
                    width: 60px;
                    transform-origin: 0 50%;
                    border-radius: 0 100px 100px 0;
                    animation: rotate-circle 4.25s ease-in;
                }

                &::before,
                &::after {
                    content: '';
                    height: 100px;
                    position: absolute;
                    background: #FFFFFF;
                    transform: rotate(-45deg);
                }

                .icon-line {
                    height: 5px;
                    background-color: #4CAF50;
                    display: block;
                    border-radius: 2px;
                    position: absolute;
                    z-index: 10;

                    &.line-tip {
                        top: 46px;
                        left: 14px;
                        width: 25px;
                        transform: rotate(45deg);
                        animation: icon-line-tip 0.75s;
                    }

                    &.line-long {
                        top: 38px;
                        right: 8px;
                        width: 47px;
                        transform: rotate(-45deg);
                        animation: icon-line-long 0.75s;
                    }
                }

                .icon-circle {
                    top: -4px;
                    left: -4px;
                    z-index: 10;
                    width: 80px;
                    height: 80px;
                    border-radius: 50%;
                    position: absolute;
                    box-sizing: content-box;
                    border: 4px solid rgba(76, 175, 80, .5);
                }

                .icon-fix {
                    top: 8px;
                    width: 5px;
                    left: 26px;
                    z-index: 1;
                    height: 85px;
                    position: absolute;
                    transform: rotate(-45deg);
                    background-color: #FFFFFF;
                }
            }
        }

        @keyframes rotate-circle {
            0% {
                transform: rotate(-45deg);
            }

            5% {
                transform: rotate(-45deg);
            }

            12% {
                transform: rotate(-405deg);
            }

            100% {
                transform: rotate(-405deg);
            }
        }

        @keyframes icon-line-tip {
            0% {
                width: 0;
                left: 1px;
                top: 19px;
            }

            54% {
                width: 0;
                left: 1px;
                top: 19px;
            }

            70% {
                width: 50px;
                left: -8px;
                top: 37px;
            }

            84% {
                width: 17px;
                left: 21px;
                top: 48px;
            }

            100% {
                width: 25px;
                left: 14px;
                top: 45px;
            }
        }

        @keyframes icon-line-long {
            0% {
                width: 0;
                right: 46px;
                top: 54px;
            }

            65% {
                width: 0;
                right: 46px;
                top: 54px;
            }

            84% {
                width: 55px;
                right: 0px;
                top: 35px;
            }

            100% {
                width: 47px;
                right: 8px;
                top: 38px;
            }
        }
    </style>
</head>

<body>
    <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="card col-md-4 bg-white shadow-md p-5">
            <div class="mb-4 text-center">
                <h1 style="font-size: 70px" id="timer">{{$timer}}</h1>
            </div>
            <div class="text-center">
                <h3>{{$message}}</h3>
                {{-- <p>Klik Tombol Di bawah Jika Pengerjaan Sudah Selesai</p> --}}
                {{-- <a class="btn btn-success btn-block" href="{{$link}}">Selesai</a> --}}
                <form class="text-center mt-5" action="{{route('forms.updatestatus.pengerjaan.selesai',[$sid])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                      <label for="petugas" class="text-left">Upload Bukti Dan Klik Tombol Submit Jika Sudah Selesai</label>
                      <input type="file" name="bukti" class="form-control" id="petugas" placeholder="Masukan Petugas">
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="mt-2 btn btn-block btn-success">Submit</button>
                    </div>
                  </form>

            </div>
            <hr>
            <div class="text-center">
            {{-- @dump($isi->is_pending) --}}
            @if ($isi->is_pending == 1)
                <span class="badge bg-secondary">Pengerjaan Tertunda</span>
                <br>
                <p>Keterangan : {{$isi->keterangan}}</p>
            @else
            <div class="text-center">
                <div class="row">
                    {{-- <button type="button" class="mt-2 btn btn-block btn-warning">Pending Pengerjaan</button> --}}
                    <button type="button" class="mt-2 btn btn-block btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Tunda Pengerjaan
                      </button>
                </div>
            </div>
            @endif
            </div>
        </div>

        <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="text-center mt-5" action="{{route('forms.updatestatus.pengerjaan.pending',[$sid])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Keterangan Penundaan Pengerjaan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <input type="text" name="keterangan" class="form-control" required>
                </div>
                <div class="modal-footer">
                {{-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> --}}
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
      </div>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script>
            $("button").click(function () {
            $(".check-icon").hide();
            setTimeout(function () {
                $(".check-icon").show();
            }, 10);
            });
        </script>
        <script>
            // Mendapatkan waktu awal dari div timer
      let timerDiv = document.getElementById("timer");
      let startTime = timerDiv.textContent.split(":").map(Number);
      let hours = startTime[0];
      let minutes = startTime[1];
      let seconds = startTime[2];
    
      // Fungsi untuk menambahkan nol di depan angka jika kurang dari 10
      function pad(num) {
        return (num < 10 ? "0" : "") + num;
      }
    
      // Fungsi untuk mengupdate timer setiap detik
      function updateTimer() {
        seconds++;
        if (seconds >= 60) {
          seconds = 0;
          minutes++;
          if (minutes >= 60) {
            minutes = 0;
            hours++;
          }
        }
        // Menampilkan timer dalam format HH:MM:SS
        timerDiv.textContent = pad(hours) + ":" + pad(minutes) + ":" + pad(seconds);
      }
    
      // Memulai timer
      setInterval(updateTimer, 1000);
        </script>
</body>

</html>