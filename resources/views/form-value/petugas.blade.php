<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Komplain | {{ Utility::getsettings('app_name') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon"
        href="{{ Utility::getsettings('favicon_logo') ? Storage::url('app-logo/app-favicon-logo.png') : '' }}"
        type="image/png">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .mb-oo{
                margin-bottom: 300px
            }
        </style>
    </head>

    <body>
        <div class="mt-5 mb-oo d-flex justify-content-center align-items-center">
            {{-- <div class="card col-10 col-md-6 bg-white shadow-md p-5">

                <hr>
                <form class="text-center">
                    <div class="form-group">
                      <label for="petugas" class="text-left">Petugas</label>
                      <input type="text" class="form-control" id="petugas" placeholder="Masukan Petugas">
                    </div>
                    <button type="submit" class="mt-2 btn btn-block btn-success">Simpan & Kerjakan</button>
                  </form>
            </div> --}}
            <div class="card col-11 col-md-6">
                <div class="card-header px-5 py-4">
                    <h5> {{ $formValue->Form->title }}

                    </h5>
                </div>
                {{-- @php
                    $formRule = App\Models\formRule::where('form_id', $formValue->form_id)->get();
                    $hideFields = [];

                    foreach ($formRule as $rule) {
                        $thenJsonData = json_decode($rule->then_json, true);
                        if (is_array($thenJsonData)) {
                            foreach ($thenJsonData as $condition) {
                                if ($condition['else_rule_type'] === 'hide') {
                                    $hideFields = array_merge($hideFields, $condition['else_field_name']);
                                }
                            }
                        }
                    }
                @endphp --}}
                <div class="card-body px-5 py-4">
                    <div class="view-form-data">
                        <div class="row">
                            @foreach ($array as $keys => $rows)
                                @foreach ($rows as $row_key => $row)
                                    @php
                                        // $fieldHidden = false;
                                        // if (in_array($row->name, $hideFields)) {
                                        //     $fieldHidden = true;
                                        // }
                                    @endphp
                                    @if ($row->type == 'checkbox-group')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    <p>
                                                    <ul>
                                                        @foreach ($row->values as $key => $options)
                                                            @if (isset($options->selected))
                                                                <li>
                                                                    <label>{{ $options->label }}</label>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'file')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    <p>
                                                        @if (property_exists($row, 'value'))
                                                            @if ($row->value)
                                                                @php
                                                                    $allowed_extensions = ['pdf', 'pdfa', 'fdf', 'xdp', 'xfa', 'pdx', 'pdp', 'pdfxml', 'pdxox', 'xlsx', 'csv', 'xlsm', 'xltx', 'xlsb', 'xltm', 'xlw'];
                                                                @endphp
                                                                @if ($row->multiple)
                                                                    <div class="row">
                                                                        @if (App\Facades\UtilityFacades::getsettings('storage_type') == 'local')
                                                                            @foreach ($row->value as $img)
                                                                                <div class="col-6">
                                                                                    @php
                                                                                        $fileName = explode('/', $img);
                                                                                        $fileName = end($fileName);
                                                                                    @endphp
                                                                                    @if (in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions))
                                                                                        @php
                                                                                            $fileName = explode('/', $img);
                                                                                            $fileName = end($fileName);
                                                                                        @endphp
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="{{ asset('storage/app/' . $img) }}"
                                                                                            type="image"
                                                                                            download="">{!! substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : '') !!}</a>
                                                                                    @else
                                                                                        <img src="{{ Storage::exists($img) ? asset('storage/app/' . $img) : Storage::url('not-exists-data-images/78x78.png') }}"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnail">
                                                                                            <a href="{{Storage::exists($img)}}" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    @endif
                                                                                </div>
                                                                            @endforeach
                                                                        @else
                                                                            @foreach ($row->value as $img)
                                                                                <div class="col-6">
                                                                                    @php
                                                                                        $fileName = explode('/', $img);
                                                                                        $fileName = end($fileName);
                                                                                    @endphp
                                                                                    @if (in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions))
                                                                                        @php
                                                                                            $fileName = explode('/', $img);
                                                                                            $fileName = end($fileName);
                                                                                        @endphp
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="{{ Storage::url($img) }}"
                                                                                            type="image"
                                                                                            download="">{!! substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : '') !!}</a>
                                                                                    @else
                                                                                        <img src="{{ Storage::url($img) }}"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnail">
                                                                                            <a href="{{Storage::exists($img)}}" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    @endif
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                @else
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            @if ($row->subtype == 'fineuploader')
                                                                                @if (App\Facades\UtilityFacades::getsettings('storage_type') == 'local')
                                                                                    @if ($row->value[0])
                                                                                        @foreach ($row->value as $img)
                                                                                            @php
                                                                                                $fileName = explode('/', $img);
                                                                                                $fileName = end($fileName);
                                                                                            @endphp
                                                                                            @if (in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions))
                                                                                                <a class="my-2 btn btn-info"
                                                                                                    href="{{ asset('storage/app/' . $img) }}"
                                                                                                    type="image"
                                                                                                    download="">{!! substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : '') !!}</a>
                                                                                            @else
                                                                                                <img src="{{ Storage::exists($img) ? asset('storage/app/' . $img) : Storage::url('not-exists-data-images/78x78.png') }}"
                                                                                                    class="mb-2 img-responsive img-thumbnail">
                                                                                                    <a href="{{asset('storage/app/' . $img)}}" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @else
                                                                                    @if ($row->value[0])
                                                                                        @foreach ($row->value as $img)
                                                                                            @php
                                                                                                $fileName = explode('/', $img);
                                                                                                $fileName = end($fileName);
                                                                                            @endphp
                                                                                            @if (in_array(pathinfo($img, PATHINFO_EXTENSION), $allowed_extensions))
                                                                                                <a class="my-2 btn btn-info"
                                                                                                    href="{{ Storage::url($img) }}"
                                                                                                    type="image"
                                                                                                    download="">{!! substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : '') !!}</a>
                                                                                            @else
                                                                                                <img src="{{ Storage::url($img) }}"
                                                                                                    class="mb-2 img-responsive img-fluid img-thumbnail">
                                                                                                    <a href="{{Storage::exists($img)}}" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endif
                                                                            @else
                                                                                @if (App\Facades\UtilityFacades::getsettings('storage_type') == 'local')
                                                                                    @if (in_array(pathinfo($row->value, PATHINFO_EXTENSION), $allowed_extensions))
                                                                                        @php
                                                                                            $fileName = explode('/', $row->value);
                                                                                            $fileName = end($fileName);
                                                                                        @endphp
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="{{ asset('storage/app/' . $row->value) }}"
                                                                                            type="image"
                                                                                            download="">{!! substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : '') !!}</a>
                                                                                    @else
                                                                                        <img src="{{ Storage::exists($row->value) ? asset('storage/app/' . $row->value) : Storage::url('not-exists-data-images/78x78.png') }}"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnailss">
                                                                                            <a href="{{asset('storage/app/' . $row->value)}}" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    @endif
                                                                                @else
                                                                                    @if (in_array(pathinfo($row->value, PATHINFO_EXTENSION), $allowed_extensions))
                                                                                        @php
                                                                                            $fileName = explode('/', $row->value);
                                                                                            $fileName = end($fileName);
                                                                                        @endphp
                                                                                        <a class="my-2 btn btn-info"
                                                                                            href="{{ Storage::url($row->value) }}"
                                                                                            type="image"
                                                                                            download="">{!! substr($fileName, 0, 30) . (strlen($fileName) > 30 ? '...' : '') !!}</a>
                                                                                    @else
                                                                                        <img src="{{ Storage::url($row->value) }}"
                                                                                            class="mb-2 img-responsive img-fluid img-thumbnailss">
                                                                                            <a href="{{Storage::exists($row->value)}}" target="_blank" rel="noopener noreferrer">Lihat Foto</a>
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'header')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <{{ $row->subtype }}>
                                                        {{ html_entity_decode($row->label) }}
                                                        </{{ $row->subtype }}>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'paragraph')
                                        
                                    @elseif($row->type == 'radio-group')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    <p>
                                                        @foreach ($row->values as $key => $options)
                                                            @if (isset($options->selected))
                                                                {{ $options->label }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'select')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif

                                                    <p>
                                                        @foreach ($row->values as $options)
                                                            @if (isset($options->selected))
                                                                {{ $options->label }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif ($row->type == 'autocomplete')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    <p>
                                                        {{ $row->value }}
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'number')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <b>{{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                </b>
                                                <p>
                                                    {{ isset($row->value) ? $row->value : '' }}
                                                </p>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'text')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    {{-- @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif --}}
                                                    @if ($row->subtype == 'color')
                                                        <div class="p-2"
                                                            style="background-color: {{ $row->value }};">
                                                        </div>
                                                    @else
                                                        <p style="font-weight: 700">
                                                            {{ isset($row->value) ? $row->value : '' }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'date')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    <p style="font-weight: 700">
                                                        {{ isset($row->value) ? date('d-m-Y', strtotime($row->value)) : '' }}
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'textarea')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    @if ($row->subtype == 'ckeditor')
                                                        {!! isset($row->value) ? $row->value : '' !!}
                                                    @else
                                                        <p style="font-weight: 700">
                                                            {{ isset($row->value) ? $row->value : '' }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'starRating')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    @php
                                                        $attr = ['class' => 'form-control'];
                                                        if ($row->required) {
                                                            $attr['required'] = 'required';
                                                        }
                                                        $value = isset($row->value) ? $row->value : 0;
                                                        $noOfStar = isset($row->number_of_star) ? $row->number_of_star : 5;
                                                    @endphp
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    <p>
                                                    <div id="{{ $row->name }}" class="starRating"
                                                        data-value="{{ $value }}"
                                                        data-no_of_star="{{ $noOfStar }}">
                                                    </div>
                                                    <input type="hidden" name="{{ $row->name }}"
                                                        value="{{ $value }}">
                                                    </p>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'SignaturePad')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    @php
                                                        $attr = ['class' => 'form-control'];
                                                        if ($row->required) {
                                                            $attr['required'] = 'required';
                                                        }
                                                        $value = isset($row->value) ? $row->value : 0;
                                                        $noOfStar = isset($row->number_of_star) ? $row->number_of_star : 5;
                                                    @endphp
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}
                                                    @if ($row->required)
                                                        <span class="text-danger align-items-center">*</span>
                                                    @endif
                                                    @if (property_exists($row, 'value'))
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <img src="{{ asset(Storage::url($row->value)) }}">
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'break')
                                        
                                    @elseif($row->type == 'location')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {!! Form::label('location_id', 'Location:') !!}
                                                    <iframe width="100%" height="260" frameborder="0"
                                                        scrolling="no" marginheight="0" marginwidth="0"
                                                        src="https://maps.google.com/maps?q={{ $row->value }}&hl=en&z=14&amp;output=embed">
                                                    </iframe>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'video')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="col-12">
                                                <div class="form-group">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}<br>
                                                    <a href="{{ route('selfie.image.download', $formValue->id) }}">
                                                        <button class="btn btn-primary p-2"
                                                            id="downloadButton">{{ __('Download Video') }}</button></a>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @elseif($row->type == 'selfie')
                                        {{-- @if (!$fieldHidden) --}}
                                            <div class="row">
                                                <div class="col-12">
                                                    {{ Form::label($row->name, $row->label, ['class' => 'form-label']) }}<br>
                                                    <img
                                                        src=" {{ Illuminate\Support\Facades\File::exists(Storage::path($row->value)) ? Storage::url($row->value) : Storage::url('app-logo/78x78.png') }}"class="img-responsive img-thumbnailss mb-2">
                                                    <br>
                                                    <a href="{{ route('selfie.image.download', $formValue->id) }}">
                                                        <button class="btn btn-primary p-2"
                                                            id="downloadButton">{{ __('Download Image') }}</button></a>
                                                </div>
                                            </div>
                                        {{-- @endif --}}
                                    @else
                                        
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card-footer px-5 py-4">
                    <form class="text-center" action="{{route('forms.updatestatus.petugas.post',[$sid])}}" method="post">
                        @csrf
                        <div class="form-group">
                          <label for="petugas" class="text-left">Petugas</label>
                          <input type="text" name="petugas" class="form-control" id="petugas" placeholder="Masukan Petugas">
                        </div>
                        <button type="submit" class="mt-2 btn btn-block btn-success">Simpan & Kerjakan</button>
                      </form>
                </div>
            </div>
            <script>
                $("button").click(function () {
  $(".check-icon").hide();
  setTimeout(function () {
    $(".check-icon").show();
  }, 10);
});
            </script>
    </body>

</html> 