@php
    $currantColumn = [];
@endphp
<table>
    <tbody>
        @foreach ($formvalues as $key => $formValue)
            @php
                $column = $formValue->columns();
            @endphp
            @if ($currantColumn != $column)
                @php
                    $currantColumn = $column;
                @endphp
                @if ($key != 0)
                    <tr></tr>
                @endif
                <tr>
                    @foreach ($currantColumn as $value)
                        <th>{{ $value }}</th>
                    @endforeach
                    {{-- <th>{{ __('Amount') }}</th>
                    <th>{{ __('Transaction ID') }}</th>
                    <th>{{ __('Created At') }}</th> --}}
                    <th>{{ __('WA Pelapor') }}</th>
                    <th>{{ __('Laporan Masuk') }}</th>
                    <th>{{ __('Laporan Dikerjakan') }}</th>
                    <th>{{ __('Laporan Selesai') }}</th>
                </tr>

            @endif
            <tr>
                @foreach (json_decode($formValue->json) as $jsons)
                    @foreach ($jsons as $json)
                        @if (isset($json->value) || isset($json->values))
                            @if (isset($json->value))
                                @if ($json->type == 'starRating')
                                    <td>{{ isset($json->value) ? $json->value : '-' }}</td>
                                @elseif ($json->type == 'button')
                                    <td> </td>
                                @elseif ($json->type == 'date')
                                    <td>{{ isset($json->value) ? $json->value : '-' }}</td>
                                @elseif ($json->type == 'number')
                                    <td>{{ isset($json->value) ? $json->value : '-' }}</td>
                                @elseif ($json->type == 'text')
                                    <td>{{ isset($json->value) ? $json->value : '-' }}</td>
                                @elseif ($json->type == 'textarea')
                                    <td>{{ isset($json->value) ? $json->value : '-' }}</td>
                                @elseif ($json->type == 'hidden')
                                    @if ($json->name != 'type')
                                    <td>{{ isset($json->value) ? $json->value : '-' }}</td>
                                    @endif
                                @elseif ($json->type == 'autocomplete')
                                    <td>{{ isset($json->value) ? $json->value : null }}</td>
                                @elseif ($json->type == 'location')
                                    @if ($json->value != '')
                                        <td>{{ isset($json->value) }}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                @endif
                            @elseif (isset($json->values))
                                @php
                                    $value = '';
                                @endphp
                                @foreach ($json->values as $subData)
                                    @if ($json->type == 'checkbox-group')
                                        @if (isset($subData->selected))
                                            @php  $value .= $subData->label . ',' @endphp
                                        @endif
                                    @elseif ($json->type == 'radio-group')
                                        @if (isset($subData->selected))
                                            @php  $value .= $subData->label . ',' @endphp
                                        @endif
                                    @elseif($json->type == 'select')
                                        @if (isset($subData->selected))
                                            @php  $value .= $subData->label . ',' @endphp
                                        @endif
                                    @endif
                                @endforeach
                                @php  $value = rtrim($value, ',') @endphp
                                <td>{{ $value ? $value : '' }}</td>
                            @endif

                        @elseif ($json->type == 'header')
                            @if (isset($json->selected) && $json->selected)
                                {{ intval($json->number_of_control) }}
                                <td> {{ __('N/A') }}</td>
                            @else
                                <td>{{ isset($json->value) ? $json->value : '' }}</td>
                            @endif
                        @else
                        @endif
                    @endforeach


                    <td>{{ isset($formValue->nomorwa_pelapor) ? $formValue->nomorwa_pelapor : '-' }}</td>
                    <td>{{ isset($formValue->datetime_masuk) ? $formValue->datetime_masuk : '-' }}</td>
                    <td>{{ isset($formValue->datetime_pengerjaan) ? $formValue->datetime_pengerjaan : '-' }}</td>
                    <td>{{ isset($formValue->datetime_selesai) ? $formValue->datetime_selesai : '-' }}</td>

                    
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
