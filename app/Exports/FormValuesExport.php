<?php

namespace App\Exports;

use App\Models\Form;
use App\Models\FormValue;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FormValuesExport implements FromView
{
    public $request;
    public $startDate;
    public $endDate;


    public function __construct($request , $startDate , $endDate)
    {
        $this->request      = $request;
        $this->startDate    = $startDate;
        $this->endDate      = $endDate;

    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $formValues = FormValue::select('*')->where('form_id', $this->request->form_id);

        if ($this->startDate && $this->endDate) {
            $formValues->whereBetween('form_values.created_at', [$this->startDate, $this->endDate]);
        }
        
        $formValues = $formValues->get();
        
        $totalSum = 0;
        $totalCount = 0;
        
        foreach ($formValues as $formV) {
            // Pastikan $formV['json'] adalah sebuah string JSON yang valid
            $jsonData = json_decode($formV['json'], true);
            
            // Periksa apakah $jsonData adalah array atau objek
            if (is_array($jsonData)) {
                foreach ($jsonData as $group) {
                    if (isset($group['type']) && ($group['type'] === 'radio-group' || $group['type'] === 'checkbox-group' || $group['type'] === 'select')) {
                        if (isset($group['values']) && is_array($group['values'])) {
                            foreach ($group['values'] as $value) {
                                if (isset($value['selected']) && $value['selected'] == 1) {
                                    if (isset($group['className']) && $group['className'] === 'tanya') {
                                        $allValues[trim(str_replace("\n ", '', $group['label']))][] =  $value['label'] . ' (' . $value['value'].')';
                                        $totalSum += (int)$value['value'];
                                        $totalCount++;
                                    } else {
                                        $allValues[trim(str_replace("\n ", '', $group['label']))][] =  $value['label'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Jika tidak ada nilai dengan kelas 'tanya', mengembalikan 0
        if ($totalCount === 0) {
            $average = 0;
        } else {
            // Menghitung rata-rata dengan membagi jumlah total dengan jumlah indeks 'tanya'
            $average = $totalSum / $totalCount;
        }
        
        // Masukkan nilai rata-rata ke dalam properti $formValues
        $formValues->average = $average;

        // return $formValues;


        $formtype = Form::where('id',$this->request->form_id)->first();
        if ($formtype->type == 'survey') {
            return view('export.formvaluesurvey', [
                'formvalues' => $formValues,
                'average' => $average ,
            ]);
        }else if($formtype->type == 'komplain'){
            return view('export.formvaluekomplain', [
                'formvalues' => $formValues
            ]);
        }else{
            return view('export.formvalue', [
                'formvalues' => $formValues
            ]);
        }
    }
}
