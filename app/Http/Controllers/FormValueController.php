<?php

namespace App\Http\Controllers;

use App\DataTables\FormValuesDataTable;
use App\Exports\FormValuesExport;
use App\Facades\UtilityFacades;
use App\Models\Form;
use App\Models\FormValue;
use App\Models\UserForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;


class ChartDataExport implements FromCollection
{
    use Exportable;

    protected $chartdata;

    public function __construct($chartdata)
    {
        $this->chartdata = $chartdata;
    }

    public function collection()
    {
        $data = [];

        foreach ($this->chartdata as $item) {
            $rowData = [
                'Label' => $item['label']
            ];

            foreach ($item['options'] as $optionLabel => $optionValue) {
                $rowData[$optionLabel] = $optionLabel . ' ('.$optionValue.')';
            }

            $data[] = $rowData;
        }

        return collect($data);
    }
}
class FormValueController extends Controller
{
    public function index(FormValuesDataTable $dataTable)
    {
        if (\Auth::user()->can('manage-submitted-form')) {
            $forms = Form::all();
            return $dataTable->render('form-value.index', compact('forms'));
        } else {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function showSubmitedForms($form_id, FormValuesDataTable $dataTable)
    {
        if (\Auth::user()->can('manage-submitted-form')) {
            $forms          = Form::all();
            $chartData      = UtilityFacades::dataChart($form_id);
            $formsDetails  = Form::find($form_id);
            // dd($chartData);
            return $dataTable->with('form_id', $form_id)->render('form-value.view-submited-form', compact('forms', 'chartData', 'formsDetails'));
            // return Excel::download(new ChartDataExport($chartData), 'chartdata.xlsx');
        } else {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function exportData($form_id){
        $chartData      = UtilityFacades::dataChart($form_id);
        return Excel::download(new ChartDataExport($chartData), 'data.xlsx');
    }

    public function show($id)
    {
        if (\Auth::user()->can('show-submitted-form')) {
            try {
                $formValue = FormValue::find($id);
                $array      = json_decode($formValue->json);
            } catch (\Throwable $th) {
                return redirect()->back()->with('errors', $th->getMessage());
            }
            return view('form-value.view', compact('formValue', 'array'));
        } else {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function edit($id)
    {
        $usr             = \Auth::user();
        $userRole       = $usr->roles->first()->id;
        $formValue      = FormValue::find($id);
        $formallowededit = UserForm::where('role_id', $userRole)->where('form_id', $formValue->form_id)->count();
        if (\Auth::user()->can('edit-submitted-form') && $usr->type == 'Admin') {
            $array  = json_decode($formValue->json);
            $form   = $formValue->Form;
            $frm    = Form::find($formValue->form_id);
            return view('form.fill', compact('form', 'formValue', 'array'));
        } else {
            if (\Auth::user()->can('edit-submitted-form') && $formallowededit > 0) {
                $formValue = FormValue::find($id);
                $array      = json_decode($formValue->json);
                $form       = $formValue->Form;
                $frm        = Form::find($formValue->form_id);
                return view('form.fill', compact('form', 'formValue', 'array'));
            } else {
                return redirect()->back()->with('failed', __('Permission denied.'));
            }
        }
    }

    public function destroy($id)
    {
        if (\Auth::user()->can('delete-submitted-form')) {
            FormValue::find($id)->delete();
            return redirect()->back()->with('success',  __('Form successfully deleted.'));
        } else {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function downloadPdf($id)
    {
        $formValue = FormValue::where('id', $id)->first();
        if ($formValue) {
            $formValue->createPDF();
        } else {
            $formValue = FormValue::where('id', '=', $id)->first();
            if (!$formValue) {
                $id         = Crypt::decryptString($id);
                $formValue = FormValue::find($id);
            }
            if ($formValue) {
                $formValue->createPDF();
            } else {
                return redirect()->route('home')->with('error', __('File is not exist.'));
            }
        }
    }

    public function export(Request $request)
    {
        $form = Form::find($request->form_id);
        return Excel::download(new FormValuesExport($request), $form->title . '.csv');
    }

    public function downloadCsv2($id)
    {
        $formValue = FormValue::where('id', '=', $id)->first();
        if (!$formValue) {
            $id         = Crypt::decryptString($id);
            $formValue = FormValue::find($id);
        }
        if ($formValue) {
            $formValue->createCSV2();
            return response()->download(storage_path('app/public/csv/Survey_' . $formValue->id . '.xlsx'))->deleteFileAfterSend(true);
        } else {
            return redirect()->route('home')->with('error', __('File is not exist.'));
        }
    }

    public function exportXlsx(Request $request)
    {
        if($request->select_date != ''){

            $form           = Form::find($request->form_id);
            $dateRange  = $request->select_date;
            list($startDate, $endDate) = array_map('trim', explode('to', $dateRange));
            $chartData      = UtilityFacades::dataChart($request->form_id);
        }
        else{
            $form  = Form::find($request->form_id);
            $startDate = '';
            $endDate = '';
            $chartData      = UtilityFacades::dataChart($request->form_id);
        }

        $new_string = preg_replace('/[^a-zA-Z0-9\s]/', '', $form->title);
        // dd($chartData);
        // return Excel::download(new ChartDataExport($chartData), 'data.xlsx');
        return Excel::download(new FormValuesExport($request , $startDate , $endDate), $new_string . '.xlsx');
        // $shellexec = substr(exec('getmac'), 0, 17); 
        // dd($shellexec);
    }

    public function getGraphData(Request $request, $id)
    {
        $form       = Form::find($id);
        $chartData  = UtilityFacades::dataChart($id);
        return view('form-value.chart', compact('chartData', 'id', 'form'));
    }

    public function VideoStore(Request $request)
    {
        $file      = $request->file('media');
        $extension = $file->getClientOriginalExtension();
        $filename  = $file->store('form_video');
        $values    = $filename;
        return response()->json(['success' => 'Video uploded successfully.', 'filename' => $values]);
    }


    public function SelfieDownload($id)
    {

        $data           = FormValue::find($id);
        $json           = $data->json;
        $jsonData       = json_decode($json, true);
        $selfieValue    = null;
        foreach ($jsonData[0] as $field) {
            if ($field['type'] === 'selfie') {
                $selfieValue = $field['value'];
                break;
            } elseif ($field['type'] === 'video') {
                $selfieValue = $field['value'];
                break;
            }
        }
        if ($selfieValue === null) {
            return redirect()->back()->with('errors', __('Image Value Not Found'));
        }
        $filePath = storage_path('app/' . $selfieValue);
        return response()->download($filePath);
    }
}
